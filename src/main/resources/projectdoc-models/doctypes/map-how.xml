<?xml version='1.0'?>
<!--

    Copyright 2018-2024 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="map-how"
  base-template="standard"
  provide-type="standard-type"
  has-homepage="false"
  category="index"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSubdocumentContextProvider">
  <resource-bundle>
    <l10n>
      <name plural="How">How</name>
      <description>
        How can the impact be pursued?
      </description>
      <about>
        Describe how the impact will be pursued.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Wie">Wie</name>
      <description>
        Wie wird die Wirkung erreicht?
      </description>
      <about>
        Beschreiben Sie, wie Sie vorgehen wollen, um die Wirkung zu erreichen.
      </about>
      <type plural="Wie-Typen">Wie-Typ</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.parent">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param
            name="property-name"
            key="projectdoc.doctype.common.name" />
          <param
            name="property"
            key="projectdoc.doctype.common.parent" />
          <param name="parent-doctype">map-who, map-how</param>
        </macro>
      </value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">map-how-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            Describe how the impact will be pursued.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Beschreiben Sie, wie Sie vorgehen wollen, um die Wirkung zu
            erreichen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.map-how.whats">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.map-how.whats"/></ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:rich-text-body>
              <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-impact:projectdoc-blueprint-doctype-map-what</ac:parameter>
                <ac:parameter ac:name="buttonLabel"><at:i18n at:key="projectdoc.doctype.map-what.create.label"/></ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">map-what</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">what-table, display-table, what</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>What?</name>
          <description>
            Describe the (high-level) deliverables (aka output) required to
            achieve the impact.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Was?</name>
          <description>
            Beschreiben Sie grob die Artefakte (output), die zur Erreichung des
            Wirkung (impact) erstellt werden müssen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="impact-map" />
    <doctype-ref id="map-why" />
    <doctype-ref id="map-who" />
    <doctype-ref id="map-what" />
  </related-doctypes>

  <wizard template="no-homepage" />
</doctype>
